/**
 * Driver for AP CS A 20202 FRQ #1 - Gizmo
 */
public class ApExam2020Question1
{
   public static void main(String[] args)
   {
      System.out.format("AP CS A - 2020 - FRQ #1 - Gizmo using java version %s%n", getJavaVersion());
      OnlinePurchaseManager opm = new OnlinePurchaseManager();
      // Part A
      System.out.println(opm.countElectronicsByMaker("ABC"));
      System.out.println(opm.countElectronicsByMaker("lmnop"));
      System.out.println(opm.countElectronicsByMaker("XYZ"));
      System.out.println(opm.countElectronicsByMaker("QRP"));

      // Part B
      System.out.println(opm.hasAdjacentEqualPair());
   }

   private static String getJavaVersion()
   {
      Runtime.Version runTimeVersion = Runtime.version();
      return String.format("%s.%s.%s.%s", runTimeVersion.feature(), runTimeVersion.interim(), runTimeVersion.update(), runTimeVersion.patch());
   }

}